<?php

namespace App\Models;


class ProjCat extends \TCG\Voyager\Models\Menu
{
    protected $table = 'project_categories';
    protected $filable = ['cat_id','cat_title','slug','proj_img','builder_gal','description'];
}
