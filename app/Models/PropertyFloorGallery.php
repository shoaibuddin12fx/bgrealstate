<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyFloorGallery extends Model
{
    use HasFactory;
    protected $table = "property_floor_gallery";

}
