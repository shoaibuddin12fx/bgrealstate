<?php

namespace App\Http\Resources;

use App\Models\Project;
use App\Models\Property;
use Illuminate\Http\Resources\Json\JsonResource;

class AreaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {
        $obj->image = url('/') .'/storage/'. $obj->image;
        $project = Project::where('area_id', $obj->id)->first();
        $projects = new ProjectCollection(Project::where('area_id', $obj->id)->get());
        $property = null;
        $price = null;
        if($project){
            $property =  Property::where('project_id', $project->id)->first();
            if($property){
                $price = $property->price;
            }
        }
        $projectsCount = 0;
        if(count($projects) > 0){
            $projectsCount = count($projects);
        }

        return [
            "id" => $obj->id,
            "name" => $obj->name ,
            "description"=> $obj->description,
            "latitude"=> $obj->latitude,
            "longitude"=> $obj->longitude,
            "created_at"=> $obj->created_at,
            "image" => $obj->image,
            "price" => $price,
            "projectsCount" => $projectsCount
        ];
    }
}
