<?php

namespace App\Http\Resources;

use App\Models\Area;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AreaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Area $areas){
            return new AreaResource($areas);
        });

        return parent::toArray($request);
    }
}
