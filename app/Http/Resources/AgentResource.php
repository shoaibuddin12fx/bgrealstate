<?php

namespace App\Http\Resources;

use App\Models\Agent;
use Illuminate\Http\Resources\Json\JsonResource;

class AgentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {
        $obj->agent_image = url('/') .'/storage/'. $obj->agent_image;
        return [
            "id" => $obj->id,
            "name"=> $obj->name,
            "project_id"=> $obj->project_id,
            "agent_image"=> $obj->agent_image,
            "description"=> $obj->description,
            "status"=> $obj->status,
            "email"=> $obj->email,
            "phone"=> $obj->phone,
            "organization"=> $obj->organization,
            "ratings"=> $obj->ratings,
        ];

    }
}
