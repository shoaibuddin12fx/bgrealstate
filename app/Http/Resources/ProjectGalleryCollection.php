<?php

namespace App\Http\Resources;

use App\Models\ProjectGallery;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectGalleryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (ProjectGallery $projectGallery){
            return new ProjectGalleryResource($projectGallery);
        });

        return parent::toArray($request);
    }
}
