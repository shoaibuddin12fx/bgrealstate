<?php

namespace App\Http\Resources;

use App\Models\PaymentPlan;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaymentPlanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (PaymentPlan $paymentPlan){
            return new PaymentPlanResource($paymentPlan);
        });

        return parent::toArray($request);
    }
}
