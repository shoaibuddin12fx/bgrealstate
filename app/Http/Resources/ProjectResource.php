<?php

namespace App\Http\Resources;

use App\Models\Area;
use App\Models\PaymentPlan;
use App\Models\Project;
use App\Models\ProjectGallery;
use App\Models\ProjectType;
use App\Models\Property;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {
        $obj->image = url('/') .'/storage/'. $obj->image;
        $gallery = new ProjectGalleryCollection( ProjectGallery::where('project_id', $obj->id)->get() );
        $paymentPlan = new PaymentPlanCollection( PaymentPlan::where('project_id', $obj->id)->get() );
        $projectType = ProjectType::where('id', $obj->project_type_id)->first();
        $typeOfProject = '';
        if($projectType){
            $typeOfProject = $projectType->slug;
        }

        return [
            "id" => $obj->id,
            "title" => $obj->title ,
            "address" => $obj->address ,
            "area_id" => $obj->area_id ,
            "short_description" => $obj->short_description ,
            "description" => $obj->description,
            "company_id" => $obj->company_id ,
            "floor" => $obj->floor ,
            "delivery_date" => $obj->delivery_date ,
            "image" => $obj->image,
            "video" => $obj->video,
            "status" => $obj->status ,
            "property_type_id" => $obj->property_type_id ,
            "feature" => $obj->feature ,
            "latitude" => $obj->latitude ,
            "longitude" => $obj->longitude ,
            "created_at" => $obj->created_at ,
            "builder_id" => $obj->builder_id ,
            "project_type_id" => $obj->project_type_id,
            "broucher" => $obj->broucher,
            "category_id" => $obj->category_id,
            "display_order" =>  $obj->display_order,
            "gallery" => $gallery,
            "paymentPlans" => $paymentPlan,
            "projectType" => $typeOfProject

        ];
    }


}
