<?php

namespace App\Http\Resources;

use App\Models\Agent;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AgentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Agent $agent){
            return new AgentResource($agent);
        });

        return parent::toArray($request);
    }
}
