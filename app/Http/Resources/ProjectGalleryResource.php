<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectGalleryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {

        $obj->image = url('/') .'/storage/'. $obj->image;
        $obj->type = $obj->type == 'option2' ? 'floor' : 'gallery';

        return [
            "id" => $obj->id,
            "project_id" => $obj->project_id,
            "image" =>  $obj->image,
            "created_at" =>  $obj->created_at,
            "type" =>  $obj->type
        ];
    }
}
