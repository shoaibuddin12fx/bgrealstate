<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {
        return [
            "id" => $obj->id,
            "title" => $obj->title ,
            "subtitle"=> $obj->subtitle,
            "text"=> $obj->text,
            "project_id"=> $obj->project_id,
            "created_at"=> $obj->created_at,
        ];
    }
}
