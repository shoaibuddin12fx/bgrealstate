<?php

namespace App\Http\Resources;

use App\Models\PropertyGallery;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyGalleryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (PropertyGallery $propertyGallery){
            return new PropertyGalleryResource($propertyGallery);
        });

        return parent::toArray($request);
    }
}
