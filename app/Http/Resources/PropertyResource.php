<?php

namespace App\Http\Resources;

use App\Models\Agent;
use App\Models\Area;
use App\Models\Project;
use App\Models\PropertyGallery;
use App\PropertyType;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;

    }

    public static function toObject($obj)
    {
        $obj->image = url('/') .'/storage/'. $obj->image;

        $agent =  Agent::where('id', $obj->agent_id )->first() ? new AgentResource(Agent::where('id', $obj->agent_id )->first()) : null;
        $project = Project::where('id', $obj->project_id )->first() ? new ProjectResource(Project::where('id', $obj->project_id )->first()) : null;
        $propertyType = PropertyType::where('id', $obj->property_type_id )->first() ? new PropertyTypeResource(PropertyType::where('id', $obj->property_type_id )->first()) : null;
        $gallery = PropertyGallery::where('property_id', $obj->id)->first();
        $galleryImages = [];
        if($gallery){
           $galleryImages =  new PropertyGalleryCollection(PropertyGallery::where('property_id', $obj->id)->get());
        }
        $projects = [];
        if($project){
            $area = Area::where('id', $project->area_id)->first();
            if($area){
                $projects = new ProjectCollection( Project::where('area_id', $area->id )->get() );
            }
        }


        return [
            "id" => $obj->id,
            "title" => $obj->title ,
            "bedrooms" => $obj->bedrooms ,
            "washrooms" => $obj->washrooms ,
            "park" => $obj->park,
            "description" => $obj->description ,
            "short_description" => $obj->short_description ,
            "price" => $obj->price ,
            "diemensions" => $obj->diemensions ,
            "address" => $obj->address ,
            "city" => $obj->city ,
            "image" => $obj->image,
            "status" => $obj->status ,
            "property_type_id" => $obj->property_type_id ,
            "feature" => $obj->feature ,
            "latitude" => $obj->latitude ,
            "longitude" => $obj->longitude ,
            "created_at" => $obj->created_at ,
            "updated_at" => $obj->updated_at ,
            "project_id" => $obj->project_id,
            "agent_id" => $obj->agent_id,
            'agent' => $agent,
            'project' => $project,
            'propertyType' => $propertyType,
            'gallery' => $galleryImages,
            'nearbyProjects' => $projects,
            'filters' => $obj->filters

        ];
    }
}
