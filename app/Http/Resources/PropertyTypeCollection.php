<?php

namespace App\Http\Resources;

use App\Models\Project;
use App\PropertyType;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyTypeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (PropertyType $propertyType){
            return new PropertyTypeResource($propertyType);
        });

        return parent::toArray($request);
    }
}
