<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AgentCollection;
use App\Http\Resources\AreaCollection;
use App\Http\Resources\AreaResource;
use App\Http\Resources\PaymentPlanCollection;
use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\PropertyCollection;
use App\Http\Resources\PropertyGalleryResource;
use App\Http\Resources\PropertyResource;
use App\Http\Resources\PropertyTypeCollection;
use App\Models\Agent;
use App\Models\Area;
use App\Models\Company;
use App\Models\MainMenu;
use App\Models\PaymentPlan;

use App\Models\Post;
use App\Models\Menu;
use App\Models\PostMeta;
use App\Models\Project;
use App\Models\ProjectGallery;
use App\Models\ProjectType;
use App\Models\Property;
use App\Models\PropertyGallery;
use App\Models\Slider;
use App\Models\WhyUs;
use App\PropertyType;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //


    public function getPosts(Request $request){
        $posts = Post::query();
        $publishedPosts = $posts->where('post_status', 'publish')
                            ->where('post_type', 'post')
            ->get();
        $transformedData = $publishedPosts->transform(function ($item, $key) {
            $postmeta = collect($item->meta);
            //company
            $company = $postmeta->where('meta_key', 'company')->first();
            $company_value = "";
            if($company){
                $company_value = $company->meta_value;
            }
            $item['company'] = $company_value;
            //Views
            $views = $postmeta->where('meta_key', 'views')->first();
            $views_value = 0;
            if($views){
                $views_value = $views->meta_value;
            }
            $item['views'] = $views_value;
            return $item;
        });
        return self::success("Posts ", ['post' => $transformedData]);
    }

    public function getPostById(Request $request, $id){
        $posts = Post::where('id', $id)->where('post_status', 'publish')
            ->where('post_type', 'post')->get();
        if($posts){
            $transformedData = $posts->transform(function ($item, $key) {
                $postmeta = collect($item->meta);
                //company
                $company = $postmeta->where('meta_key', 'company')->first();
                $company_value = "";
                if($company){
                    $company_value = $company->meta_value;
                }
                $item['company'] = $company_value;
                //Views
                $views = $postmeta->where('meta_key', 'views')->first();
                $views_value = 0;
                if($views){
                    $views_value = $views->meta_value;
                }
                $item['views'] = $views_value;
                return $item;
            });
            return self::success("Post Found", ['data' => $transformedData ]);
        }
        return self::failure('Not found');

    }


    public function getMenusFromWp(Request $request){
        $menu = Menu::slug('menus')->first();

        if(!isset($menu)){
            return self::failure("No Menu");
        }
        $items = $menu->items;
        $metas = [];
        foreach($items as $item){
            $metas[$item->ID] = $item->meta;
        }




        $parentIds = [];
        $menuItems = $items->transform(function ($item, $key) use (&$parentIds, $metas) {
/**/
            if($item->parent()){
                $parentIds[] = $item->parent()->ID;
            }

            $meta = $metas[$item->ID];

            $url = collect($meta)->where('meta_key', '_menu_item_url')->first();
            if($url){
                $url = $url->value;
            }


            $obj = [
                'id' => $item->ID,
                'title' => $item->post_title,
                'routerLink' => $item->url ,
                'href' => $item->url,
                'target' =>$item->post_name,
                'parentId' => $item->parent() ? $item->parent()->ID : 0,
                'url' => $url
            ];

            return $obj;
        })->toArray();

        for($i = 0; $i < count($menuItems); $i++){
            $menuItems[$i]['hasSubMenu'] = in_array($menuItems[$i]['id'], $parentIds) ? 1 : 0;
        }

        return self::success('msg', ['menu' => $menuItems]);


    }

    public function getSliders(Request $request){
        $sliders = Slider::all();
        $sliders = $sliders->transform(function ($item, $key) {
            $item['image'] = url('/') .'/storage/'. $item['image'];


            return $item;
        })->toArray();


        return self::success("Sliders", ['data' => $sliders]);
    }

    public function getProperties(Request $request){
        $properties = Property::all();
        $properties = new PropertyCollection($properties);
        return self::success("Properties", ['data' => $properties]);
    }

    public function getProperty(Request $request, $id) {
        $property = Property::where('id', $id)->first();
        if($property){
            $propertyArray = new PropertyResource($property);
            return self::success("Property Found", ['property' => $propertyArray ]);
        }
        return self::failure("Property not found");
    }

    public function getSingleProject(Request $request, $id) {
        $project = Project::where('id', $id)->first();
        if($project){
            $projectResource = new ProjectResource($project);
            return self::success("Project Found", ['data' => $projectResource ]);
        }
        return self::failure("Project not found");
    }


    public function getOffPlanProjectType(Request $request){
        $types = ProjectType::where('slug', 'off-plan-project')->first();
        if(!isset($types)){
            return self::success("No Projects ", ['projects' => [], "count" => 0]);
        }
        $projects = Project::where('project_type_id', $types->id)->get();
        return self::success("Off-Plan Projects ", ['data' => $projects, "count" => count($projects)]);
    }

    public function getWhyUs(Request $request){
        $whyUs = WhyUs::all();
        return self::success("Why-Us", ['data' => $whyUs]);
    }

    public function getCompanyDetails(Request $request){
        $company = Company::all();
        return self::success("Company", ['data' => $company]);
    }

    public function getFooterLinks(Request $request){
        //First Column
        $properties = \App\Models\Menu::slug('properties')->first();
        if(!isset($properties)){
            return self::failure("No Menu");
        }
        $items = $properties->items;
        $metas = [];
        foreach($items as $item){
            $metas[$item->ID] = $item->meta;
        }

        $parentIds = [];
        $onlyProperties = $items->transform(function ($item, $key) use (&$parentIds, $metas) {
            /**/
            if($item->parent()){
                $parentIds[] = $item->parent()->ID;
            }

            $meta = $metas[$item->ID];

            $url = collect($meta)->where('meta_key', '_menu_item_url')->first();
            if($url){
                $url = $url->value;
            }

            $obj = [
                'id' => $item->ID,
                'title' => $item->post_title,
                'routerLink' => $item->url ,
                'href' => $item->url,
                'target' =>$item->post_name,
                'parentId' => $item->parent() ? $item->parent()->ID : 0,
                'url' => $url
            ];

            return $obj;
        })->toArray();


        //Second Column
        $properties_for_sale = \App\Models\Menu::slug('properties-for-sale')->first();
        if(!isset($properties_for_sale)){
            return self::failure("No Menu");
        }
        $items = $properties_for_sale->items;
        $metas = [];
        foreach($items as $item){
            $metas[$item->ID] = $item->meta;
        }

        $parentIds = [];
        $propertySale = $items->transform(function ($item, $key) use (&$parentIds, $metas) {
            /**/
            if($item->parent()){
                $parentIds[] = $item->parent()->ID;
            }

            $meta = $metas[$item->ID];

            $url = collect($meta)->where('meta_key', '_menu_item_url')->first();
            if($url){
                $url = $url->value;
            }

            $obj = [
                'id' => $item->ID,
                'title' => $item->post_title,
                'routerLink' => $item->url ,
                'href' => $item->url,
                'target' =>$item->post_name,
                'parentId' => $item->parent() ? $item->parent()->ID : 0,
                'url' => $url
            ];

            return $obj;
        })->toArray();


        //Three Column

        $properties_for_rent = \App\Models\Menu::slug('properties-for-rent')->first();
        if(!isset($properties_for_rent)){
            return self::failure("No Menu");
        }
        $items = $properties_for_rent->items;
        $metas = [];
        foreach($items as $item){
            $metas[$item->ID] = $item->meta;
        }

        $parentIds = [];
        $propertiesRents = $items->transform(function ($item, $key) use (&$parentIds, $metas) {
            /**/
            if($item->parent()){
                $parentIds[] = $item->parent()->ID;
            }

            $meta = $metas[$item->ID];

            $url = collect($meta)->where('meta_key', '_menu_item_url')->first();
            if($url){
                $url = $url->value;
            }

            $obj = [
                'id' => $item->ID,
                'title' => $item->post_title,
                'routerLink' => $item->url ,
                'href' => $item->url,
                'target' =>$item->post_name,
                'parentId' => $item->parent() ? $item->parent()->ID : 0,
                'url' => $url
            ];

            return $obj;
        })->toArray();



        //Fourth Column

//        $off_plan_properties = \App\Models\Menu::slug('off-plan-properties')->first();
//
//        $offPlanProperties = $off_plan_properties->items->transform(function ($item, $key)  {
//
//            $obj = [
//                'id' => $item->ID,
//                'title' => $item->post_title,
//                'routerLink' =>  $item->slug ,
//                'href' => $item->url,
//                'target' =>$item->post_name,
//                'parentId' => $item->parent() ? $item->parent()->ID : 0
//            ];
//            return $obj;
//        })->toArray();
//
//        //Fifth Column
//
//        $living_experience = \App\Models\Menu::slug('living-experience')->first();
//
//        $livingExperienceItems = $living_experience->items->transform(function ($item, $key)  {
//
//            $obj = [
//                'id' => $item->ID,
//                'title' => $item->post_title,
//                'routerLink' =>  $item->slug ,
//                'href' => $item->url,
//                'target' =>$item->post_name,
//                'parentId' => $item->parent() ? $item->parent()->ID : 0
//            ];
//            return $obj;
//        })->toArray();
//
//        //Sixth Column
//
//        $beachfront_waterfront_ = \App\Models\Menu::slug('beachfront-waterfront-properties')->first();
//
//        $beachFrontWaterFrontProperties = $beachfront_waterfront_->items->transform(function ($item, $key)  {
//
//            $obj = [
//                'id' => $item->ID,
//                'title' => $item->post_title,
//                'routerLink' =>  $item->slug ,
//                'href' => $item->url,
//                'target' =>$item->post_name,
//                'parentId' => $item->parent() ? $item->parent()->ID : 0
//            ];
//            return $obj;
//        })->toArray();
//
//        //Seventh Column

//        $projects = \App\Models\Menu::slug('projects')->first();
//
//        if(!isset($projects)){
//            return self::failure("No Menu");
//        }
//        $items = $projects->items;
//        $metas = [];
//        foreach($items as $item){
//            $metas[$item->ID] = $item->meta;
//        }
//
//        $parentIds = [];
//        $projectItems = $items->transform(function ($item, $key) use (&$parentIds, $metas) {
//            /**/
//            if($item->parent()){
//                $parentIds[] = $item->parent()->ID;
//            }
//
//            $meta = $metas[$item->ID];
//
//            $url = collect($meta)->where('meta_key', '_menu_item_url')->first();
//            if($url){
//                $url = $url->value;
//            }
//
//            $obj = [
//                'id' => $item->ID,
//                'title' => $item->post_title,
//                'routerLink' => $item->url ,
//                'href' => $item->url,
//                'target' =>$item->post_name,
//                'parentId' => $item->parent() ? $item->parent()->ID : 0,
//                'url' => $url
//            ];
//
//            return $obj;
//        })->toArray();





        return self::success("Footer", ['properties' => $onlyProperties,
                                            'propForSale' => $propertySale,
                                            'propertiesForRent' => $propertiesRents,
        ]);

    }

    public function getPropertyImages(){
        $images = PropertyGallery::all();
        $images = new PropertyGalleryResource($images);
        return self::success('Images: ', ['data' => $images]);
    }

    public function getProjectImages(){
        $images = ProjectGallery::all();
        $projectImages = $images->transform(function ($item, $key)  {
            $item['image'] = url('/') .'/storage/'. $item['image'];
            return $item;
        })->toArray();
        return self::success('Images: ', ['data' => $projectImages]);
    }

    public function getAgents(Request $request){
        $agents = Agent::all();
        $agents = new AgentCollection($agents);
        return self::success("Agents", ['data' => $agents]);
    }

    public function getProjects(Request $request){
        $projects = Project::all();
        $projects = new ProjectCollection($projects);
        return self::success("Projects", ['data' => $projects]);
    }

    public function getPropertyTypes(Request $request){
        $propertyTypes = PropertyType::all();
        $propertyTypes = new PropertyTypeCollection($propertyTypes);
        return self::success("PropertyTypes", ['data' => $propertyTypes]);
    }

    public function getAreas(Request $request){
        $areas = Area::all();
        $areas = new AreaCollection($areas);
        return self::success("Areas", ['data' => $areas]);
    }

    public function getSingleArea(Request $request, $id){
        $area = Area::where('id', $id)->first();
        if($area){
            $area = new AreaResource($area);
            return self::success("Area Found", ['area' => $area ]);
        }
        return self::failure("Area not found");
    }

    public function getPaymentPlans(Request $request){
        $plans = PaymentPlan::all();
        $plans = new PaymentPlanCollection($plans);
        return self::success("Plans: ", ['data' => $plans]);
    }
    public function getAboutUs(Request $request){
        $aboutUs = Post::query();
        $publishedAboutUs = $aboutUs->where('post_status', 'publish')
            ->where('post_type', 'post')->where('post_name', 'about-us')
            ->get();
        return self::success("Posts ", ['aboutUs' => $publishedAboutUs]);
    }


}
