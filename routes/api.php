<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => [ 'cors' ] ], function () {
    // ...
    Route::get('wp_posts', 'API\ApiController@getPosts');
    Route::get('menus', 'API\ApiController@getMenusFromWp');
    Route::get('sliders', 'API\ApiController@getSliders');
    Route::get('properties', 'API\ApiController@getProperties');
    Route::get('projects', 'API\ApiController@getProjects');
    Route::get('off-plan-projects', 'API\ApiController@getOffPlanProjectType');
    Route::get('why-us', 'API\ApiController@getWhyUs');
    Route::get('company', 'API\ApiController@getCompanyDetails');
    Route::get('footer', 'API\ApiController@getFooterLinks');
    Route::get('properties/{id}', 'API\ApiController@getProperty');
    Route::get('propertyGallery', 'API\ApiController@getPropertyImages');
    Route::get('projectGallery', 'API\ApiController@getProjectImages');
    Route::get('agents', 'API\ApiController@getAgents');
    Route::get('propertyTypes', 'API\ApiController@getPropertyTypes');
    Route::get('projects/{id}', 'API\ApiController@getSingleProject');
    Route::get('areas', 'API\ApiController@getAreas');
    Route::get('areas/{id}', 'API\ApiController@getSingleArea');
    Route::get('paymentPlans', 'API\ApiController@getPaymentPlans');
    Route::get('wp_posts/{id}', 'API\ApiController@getPostById');
    Route::get('aboutUs', 'API\ApiController@getAboutUs');



});





